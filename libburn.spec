Name:            libburn
Version:         1.5.6
Release:         1
Summary:         The underlying programming library
License:         GPLv2+
URL:             http://libburnia-project.org/
Source0:         http://files.libburnia-project.org/releases/libburn-%{version}.tar.gz
Source1:         http://files.libburnia-project.org/releases/libburn-%{version}.tar.gz.sig
Patch0000:       0000-libburn-0.6.16-multilib.patch
BuildRequires:   intltool gettext gcc

%description
Libburn is a library for writing preformatted data onto optical media: CD, DVD and BD (Blu-Ray).
libburn also offers a facility for reading data blocks from its drives without using the normal
block device i/o, which has its advantages and disadvantages.

%package         devel
Summary:         Development files of libburn
Requires:        libburn = %{version}-%{release} pkgconfig

%description     devel
Development files of libburn.

%package         help
Summary:         Help document for libburn
BuildArch:       noarch
BuildRequires:   doxygen graphviz
Provides:        %{name}-doc = %{version}-%{release}
Obsoletes:       %{name}-doc < %{version}-%{release}

%description     help
Help document for libburn.

%package -n      cdrskin
Summary:         A limited cdrecord compatibility wrapper for libburn
Requires:        %{name} = %{version}-%{release}
Requires(post):  chkconfig coreutils
Requires(preun): chkconfig

%description -n  cdrskin
cdrskin is the end-user application of libburnia. It is CLI-only and its syntax is mostly
identical to cdrecord to act as a drop-in replacement for existing front-ends.

%prep
%autosetup -n libburn-%{version} -p1

%build
%configure --disable-static
%make_build
doxygen doc/doxygen.conf

%install
%make_install
%delete_la

rm -rf $RPM_BUILD_ROOT%{_defaultdocdir}
ln -sf cdrskin $RPM_BUILD_ROOT%{_bindir}/cdrecord

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%post -n cdrskin
link=`readlink %{_bindir}/cdrecord`
if [ "$link" == "cdrskin" ]; then
  rm -f %{_bindir}/cdrecord
fi

%{_sbindir}/alternatives --install %{_bindir}/cdrecord cdrecord %{_bindir}/cdrskin 50 \
  --slave %{_mandir}/man1/cdrecord.1.gz cdrecord-cdrecordman %{_mandir}/man1/cdrskin.1.gz

%preun -n cdrskin
if [ $1 = 0 ]; then
  %{_sbindir}/alternatives --remove cdrecord %{_bindir}/cdrskin
fi

%files
%license COPYING
%doc AUTHORS COPYRIGHT README
%{_libdir}/%{name}*.so.*

%files devel
%{_includedir}/%{name}
%{_libdir}/%{name}*.so
%{_libdir}/pkgconfig/%{name}*.pc

%files help
%doc doc/html/
%{_mandir}/man1/cdrskin.1*

%files -n cdrskin
%ghost %{_bindir}/cdrecord
%{_bindir}/cdrskin

%changelog
* Fri Oct 13 2023 yaoxin <yao_xin001@hoperun.com> - 1.5.6-1
- Upgrade to 1.5.6

* Thu Jun 16 2022 SimpleUpdate Robot <tc@openeuler.org> - 1.5.4-1
- Upgrade to version 1.5.4

* Thu Jun 03 2021 wulei <wulei80@huawei.com> - 1.4.8-7
- fixes failed: no acceptable C compiler found in $PATH

* Sat Dec 21 2019 shijian <shijian16@huawei.com> - 1.4.8-6
- Package init
